#![allow(dead_code)]
#![feature(test)]
#![feature(stmt_expr_attributes)]

extern crate test;

mod algorithms;
mod data_structures;

#[cfg(test)]
mod tests {
	use super::{algorithms::*, data_structures::*};
	use test::Bencher;

	const SORTED: [i32; 10] = [1, 2, 6, 9, 23, 31, 50, 57, 69, 101];

	fn helper(alg: fn(&mut [i32])) {
		let mut unsorted = [57, 9, 6, 31, 1, 23, 101, 69, 50, 2];

		alg(&mut unsorted);

		assert_eq!(unsorted, SORTED);
	}

	#[bench]
	fn selection_sort(b: &mut Bencher) {
		b.iter(|| helper(sorting::selection::sort));
	}

	#[bench]
	fn bubble_sort(b: &mut Bencher) {
		b.iter(|| helper(sorting::bubble::sort));
	}

	#[bench]
	fn merge_sort(b: &mut Bencher) {
		b.iter(|| helper(sorting::merge::sort));
	}

	#[bench]
	fn quick_sort(b: &mut Bencher) {
		b.iter(|| helper(sorting::quick::sort));
	}

	#[bench]
	fn linked_list_insert(b: &mut Bencher) {
		let mut ll = linked_list::LinkedList::<u32>::new();

		b.iter(|| {
			ll = linked_list::LinkedList::<u32>::new();

			for i in 0..10 {
				ll.push(i);
			}
		});

		assert_eq!(ll.pop(), Some(9));
		assert_eq!(ll.len, 9);
		assert_eq!(
			format!("{:?}", ll),
			"8 -> 7 -> 6 -> 5 -> 4 -> 3 -> 2 -> 1 -> 0 -> None"
		);
	}

	#[bench]
	fn bst_insert(b: &mut Bencher) {
		let mut bt = bst::BinarySearchTree::<u32>::new();

		b.iter(|| {
			bt = bst::BinarySearchTree::<u32>::new();

			bt.insert(2);
			bt.insert(1);
			bt.insert(23);
			bt.insert(5);
			bt.insert(0);
			bt.insert(7);
			bt.insert(22);
			bt.insert(9);
			bt.insert(101);
			bt.insert(96);
			bt.insert(69);
			bt.insert(2);

			bt.remove(96);
		});

		assert!(bt.find(22));
		assert!(!bt.find(3));
		assert_eq!(format!("{:?}", bt), "(((None, 0, None), 1, None), 2, ((None, 5, (None, 7, ((None, 9, None), 22, None))), 23, ((None, 69, None), 101, None)))");
	}
}
