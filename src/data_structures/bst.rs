// useful stuff: https://users.rust-lang.org/t/binary-search-tree-node-removal-without-unsafe-code/56078/14

use std::cmp::Ordering;
use std::fmt::Debug;

pub struct Node<T> {
	left: TreeNode<T>,
	value: T,
	right: TreeNode<T>,
}

struct TreeNode<T>(Option<Box<Node<T>>>);

impl<T> Node<T>
where
	T: Copy + Debug + Ord,
{
	pub fn new(value: T) -> Self {
		Self {
			left: TreeNode(None),
			value,
			right: TreeNode(None),
		}
	}
}

pub struct BinarySearchTree<T> {
	root: TreeNode<T>,
}

impl<T> TreeNode<T>
where
	T: Copy + Debug + Ord,
{
	pub fn insert(&mut self, value: T) {
		match self.0 {
			None => self.0 = Some(Box::new(Node::new(value))),
			Some(ref mut node) => match value.cmp(&node.value) {
				Ordering::Less => node.left.insert(value),
				Ordering::Greater => node.right.insert(value),
				Ordering::Equal => {}
			},
		};
	}

	pub fn find(&self, value: T) -> bool {
		match self.0 {
			None => false,
			Some(ref node) => match value.cmp(&node.value) {
				Ordering::Less => node.left.find(value),
				Ordering::Greater => node.right.find(value),
				Ordering::Equal => true,
			},
		}
	}

	pub fn remove(&mut self, value: T) {
		let mut current = Some(self);

		let extract_min = |tree: &mut TreeNode<T>| -> Option<T> {
			let mut node = None;

			if tree.0.is_some() {
				let mut current = tree;

				while current.0.as_ref().unwrap().left.0.is_some() {
					current = &mut current.0.as_mut().unwrap().left;
				}

				let temp = current.0.take().unwrap();
				node = Some(temp.value);
				current.0 = temp.right.0;
			}

			node
		};

		loop {
			let current_ = current.as_deref_mut().unwrap();
			if let Some(ref mut node) = current_.0 {
				match node.value.cmp(&value) {
					Ordering::Less => {
						current = Some(
							#[rustfmt::skip]
							&mut current
								.take().unwrap().0.as_mut().unwrap().right,
						)
					}
					Ordering::Greater => {
						current = Some(
							#[rustfmt::skip]
							&mut current
								.take().unwrap().0.as_mut().unwrap().left,
						)
					}
					Ordering::Equal => {
						match (node.left.0.as_mut(), node.right.0.as_mut()) {
							(None, None) => current_.0 = None,
							(Some(_), None) => current_.0 = node.left.0.take(),
							(None, Some(_)) => {
								current_.0 = node.right.0.take()
							}
							(Some(_), Some(_)) => {
								current_.0.as_mut().unwrap().value =
									extract_min(&mut node.right).unwrap();
							}
						}
					}
				}
			} else {
				break;
			}
		}
	}
}

impl<T> Debug for TreeNode<T>
where
	T: Copy + Debug + Ord,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", match self.0.as_ref() {
			None => "None".to_owned(),
			Some(ref mut node) => format!(
				"({:?}, {:?}, {:?})",
				node.left,
				node.value,
				node.right
			),
		})
	}
}

impl<T> BinarySearchTree<T>
where
	T: Copy + Debug + Ord,
{
	pub fn new() -> Self {
		BinarySearchTree {
			root: TreeNode(None),
		}
	}

	pub fn insert(&mut self, value: T) {
		self.root.insert(value);
	}

	pub fn find(&self, value: T) -> bool {
		self.root.find(value)
	}

	pub fn remove(&mut self, value: T) {
		self.root.remove(value);
	}
}

impl<T> Debug for BinarySearchTree<T>
where
	T: Copy + Debug + Ord,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{:?}", self.root)
	}
}
