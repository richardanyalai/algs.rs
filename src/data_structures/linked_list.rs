// useful stuff: https://www.youtube.com/watch?v=IiDHTIsmUi4

use std::fmt::Debug;

struct Node<T> {
	pub value: T,
	next: Option<Box<Node<T>>>,
}

pub struct LinkedList<T> {
	head: Option<Box<Node<T>>>,
	pub len: usize,
}

impl<T> LinkedList<T>
where
	T: Debug,
{
	pub fn new() -> Self {
		Self { head: None, len: 0 }
	}

	pub fn push(&mut self, value: T) {
		self.head = Some(Box::new(Node {
			value,
			next: self.head.take(),
		}));
		self.len += 1;
	}

	pub fn pop(&mut self) -> Option<T> {
		self.head.take().map(|n| {
			self.len -= 1;
			self.head = n.next;
			n.value
		})
	}
}

impl<T> Debug for LinkedList<T>
where
	T: Debug,
{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let mut node = self.head.as_ref();

		loop {
			match node {
				None => break,
				Some(elem) => {
					node = elem.next.as_ref();
					write!(f, "{:?} -> ", elem.value)?
				}
			}
		}

		write!(f, "None")
	}
}
