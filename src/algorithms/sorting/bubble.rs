/// Bubble sort algorithm
/// * `array` - the unsorted array of elements to be sorted
pub fn sort(array: &mut [i32]) {
	let mut one_more_time;

	loop {
		one_more_time = false;

		for i in 1..array.len() {
			// if the value at the current index is smaller then the value at
			// the previous index
			if array[i] < array[i - 1] {
				// swap the two values
				array.swap(i - 1, i);

				println!("{:?}", array);

				// we need to run the sorting once again to see whether the
				// values are in the correct order
				one_more_time = true;
			}
		}

		if !one_more_time {
			break;
		}
	}
}
