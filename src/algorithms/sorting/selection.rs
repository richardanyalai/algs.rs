/// Selection sort algorithm
/// * `array` - the unsorted array of elements to be sorted
pub fn sort(array: &mut [i32]) {
	let mut idx;

	for i in 0..array.len() {
		// make the minimal index equal to i (start of unsorted subarray)
		idx = i;

		// search for minimal index (index of the smallest value)
		for j in (i + 1)..array.len() {
			if array[j] < array[idx] {
				idx = j
			}
		}

		// swap the value at the minimal index with the value at the start of
		// the unsorted subarray
		array.swap(i, idx);
	}
}
