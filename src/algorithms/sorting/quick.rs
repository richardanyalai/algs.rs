/// Quick sort helper function
/// * `array` - the array to be sorted
/// * `first element
/// * `hi - last element
fn helper(array: &mut [i32], lo: usize, hi: usize) {
	if lo < hi {
		let (mut i, mut j, pi) = (lo, hi, lo);

		while i < j {
			array.swap(i, j);

			while array[i] <= array[pi] && i < hi {
				i += 1;
			}

			while array[j] > array[pi] {
				j -= 1;
			}
		}

		array.swap(pi, j);

		// Sort all elements before and after the pivot index
		helper(array, lo, j - 1);
		helper(array, j + 1, hi);
	}
}

/// Quick sort algorithm
/// * `array` - the unsorted array of elements to be sorted
pub fn sort(array: &mut [i32]) {
	helper(array, 0, array.len() - 1)
}
