/// Merge sort helper function
/// * `array` - the array to be sorted
/// * `l` - left index
/// * `r` - right index
fn helper(array: &mut [i32], l: usize, r: usize) {
	if l < r {
		// getting middle index
		let m = l + (r - l) / 2;

		// sort both first and second halves of the array
		helper(array, l, m);
		helper(array, m + 1, r);

		// closure for merging the two temporary arrays
		// l - left index
		// m -middle index
		// r - right index
		let _helper = |array: &mut [i32], l: usize, m: usize, r: usize| {
			let n1 = m - l + 1;
			let n2 = r - m;

			// divide the subarray into two subarrays
			let mut left = vec![0i32; n1];
			let mut right = vec![0i32; n2];

			left[..n1].clone_from_slice(&array[l..(n1 + l)]);
			right[..n2].clone_from_slice(&array[(m + 1)..(n2 + 1 + m)]);

			let (mut i, mut j, mut k) = (0, 0, l);

			while i < n1 && j < n2 {
				if left[i] <= right[j] {
					array[k] = left[i];
					i += 1;
				} else {
					array[k] = right[j];
					j += 1;
				}
				k += 1;
			}

			// copy the remaining elements of the left subarray
			while i < n1 {
				array[k] = left[i];
				i += 1;
				k += 1;
			}

			// copy the remaining elements of the right subarray
			while j < n2 {
				array[k] = right[j];
				j += 1;
				k += 1;
			}
		};
		_helper(array, l, m, r);
	}
}

/// Merge sort algorithm
/// * `array` - the unsorted array of elements to be sorted
pub fn sort(array: &mut [i32]) {
	helper(array, 0, array.len() - 1);
}
